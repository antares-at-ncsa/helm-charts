# Default values for Helm deployments of the ANTARES stack.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

## Provide a name to use instead of `antares` for `app:` labels.
##
nameOverride: ""

## Provide a name to substitute for the full name of releases.
##
fullnameOverride: ""

## Reference to one or more secrets to use when pulling docker images
##
imagePullSecrets: []

## A name for the deployment environment (e.g. "production")
##
environment: "development"

## Configuration for a redis instance. By default we deploy a single-node, though you
## could configure more complicated clusters by changing configuration settings below.
## This depends on Bitnami's Redis charts, see the reference below for more details and
## a description of configuration settings.
## ref: https://github.com/bitnami/charts/tree/master/bitnami/redis
##
redis:
  ## Deploy Redis
  ##
  enabled: true
  ## Settings for the redis master node(s).
  ##
  master:
    persistence:
      enabled: false
  ## Settings for configuration of redis cluster.
  ##
  cluster:
    ## Use master-slave topology
    ##
    enabled: false
  usePassword: false


## Configure the ANTARES HTTP API.
##
api:
  enabled: true
  name: api
  replicaCount: 1
  image:
    repository: registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend
    tag: v1.10.0
    pullPolicy: IfNotPresent
  service:
    portExternal: 8080
    portInternal: 8000
    sessionAffinity: None
    type: LoadBalancer

  ## Configure an (optional) ingress for the API.
  ##
  ingress:
    enabled: false
    annotations: {}

    ## Hosts must be provided if ingress is enabled.
    ##
    hosts: []

    ## TLS configuration for API ingress.
    ## Secret will be created in the same namespace as the service.
    tls: []
    # - hosts:
    #   - api.antares.noirlab.edu
    #   secretName: antares-api-tls

  ## Pass extra environment variables to the containers of this application.
  ##
  extraEnv: []
  ## For example:
  #   - name: DATADOG_STATSD_HOST
  #     valueFrom:
  #       fieldRef:
  #         fieldPath: status.hostIP

  ## Configuration settings for the ANTARES API. These are the minimum set of config
  ## settings you'll need to pass in order to get up and running.
  ##
  config:
    DEBUG: false
    LOG_LEVEL: "INFO"

    ## Enter a full MySQL connection string, e.g. "mysql://user:pass@host:port/database"
    ##
    MYSQL_DB_URL: ""

    ## Configure the connection pool used to serve requests that access MySQL. These are
    ## okay defaults but you may want to tune to your use case.
    ##
    SQLALCHEMY_ENGINE_PARAMS:
      pool_size: 10
      max_overflow: 20
      pool_recycle: 600

    ## Specify a secret key for JWT authentication tokens.
    ##
    SECRET_KEY: ""

    ## Specify a mail server for the API to use to send emails (e.g.
    ## for forgot password flows)
    API_MAIL_SERVER: ""
    API_MAIL_PORT: 587
    API_MAIL_USE_TLS: false
    API_MAIL_USERNAME: ""
    API_MAIL_PASSWORD: ""

    ## Specify the domain that JWT authentication tokens are able to authenticate against.
    ## For example we set this value to ".antares.noirlab.edu" so that our API at
    ## "api.antares.noirlab.edu" can send a cross-domain cookie to users accessing the
    ## website at "antares.noirlab.edu".
    ##
    API_JWT_COOKIE_DOMAIN: ""

    ## Specify the base URL of the API. This is used to build relationship links in
    ## response payloads. It should be something like "https://api.antares.noirlab.edu/v1".
    ## You probably need the "/v1" suffix unless you know you don't.
    ##
    API_BASE_URL: ""

    ## Similar to the API_BASE_URL config option, this is used for building relationship
    ## links to views on the website. E.g. we set it to "https://antares.noirlab.edu".
    ##
    FRONTEND_BASE_URL: ""

    CASSANDRA_HOSTS: []
    CASSANDRA_USER: ""
    CASSANDRA_PASS: ""
    CASSANDRA_ALERT_KEYSPACE: ""
    CASSANDRA_CATALOG_KEYSPACE: ""

    ## Specify one or more elasticsearch hosts. In the simple case this is something like
    ## "elasticsearch:9200".
    ##
    ELASTICSEARCH_HOSTS: []

    ## If you need to pass authentication information, you could also use the following
    ## configuration option. Any fields passed into this dictionary are unpacked and
    ## passed into the Python `elasticsearch.Elasticsearch` constructor. See
    ## https://elasticsearch-py.readthedocs.io/en/7.10.0/api.html#elasticsearch.Elasticsearch.
    ##
    # ELASTICSEARCH_OPTIONS:
    #   http_auth:
    #     - "username"
    #     - "password"

    ## You must also specify the name of the index to be used in Elasticsearch for building
    ## the archive search database.
    ARCHIVE_INDEX_NAME: ""


## Configure the ANTARES web frontend.
##
frontend:
  enabled: true
  name: frontend
  image:
    repository: registry.gitlab.com/nsf-noirlab/csdc/antares/antares/frontend
    tag: v1.10.0
    pullPolicy: IfNotPresent

  ## Specify configuration options for the website
  ##
  # config:
  #   ANTARES_API_URL: https://api.antares.noirlab.edu
  #   ANTARES_FRONTEND_URL: https://antares.noirlab.edu

  service:
    portExternal: 80
    portInternal: 80
    sessionAffinity: None
    type: LoadBalancer

  ## Configure an (optional) ingress for the web frontend.
  ##
  ingress:
    enabled: false
    annotations: {}

    ## Hosts must be provided if ingress is enabled.
    ##
    hosts: []

    ## TLS configuration for API ingress.
    ## Secret will be created in the same namespace as the service.
    tls: []
    # - hosts:
    #   - api.antares.noirlab.edu
    #   secretName: antares-api-tls


## Configure the ANTARES index worker. This service consumes data from the output buffer
## of the pipeline and ingests it into the Elasticsearch database.
indexWorker:
  enabled: true
  name: index-worker
  image:
    repository: registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend
    tag: v1.10.0
    pullPolicy: IfNotPresent

  ## Configuration settings for the ANTARES index worker. These are the minimum set of config
  ## settings you'll need to pass in order to get up and running.
  ##
  config:
    DEBUG: false
    LOG_LEVEL: "INFO"

    ## Enter a full MySQL connection string, e.g. "mysql://user:pass@host:port/database"
    ## This should be the same database that the SLocusProperty table is stored in (
    ## which should also be the same MYSQL_DB_URL passed to the api and the pipeline).
    ##
    MYSQL_DB_URL: ""

    ## Specify one or more elasticsearch hosts. In the simple case this is something like
    ## "elasticsearch:9200".
    ##
    ELASTICSEARCH_HOSTS: []

    ## If you need to pass authentication information, you could also use the following
    ## configuration option. Any fields passed into this dictionary are unpacked and
    ## passed into the Python `elasticsearch.Elasticsearch` constructor. See
    ## https://elasticsearch-py.readthedocs.io/en/7.10.0/api.html#elasticsearch.Elasticsearch.
    ## If you do have to authenticate, the user account should have permissions to index
    ## documents and update mappings for the index specified in
    ## index_worker.config.ARCHIVE_INDEX_NAME.
    ##
    # ELASTICSEARCH_OPTIONS:
    #   http_auth:
    #     - "username"
    #     - "password"

    ## You must also specify the name of the index to be used in Elasticsearch for building
    ## the archive search database.
    ##
    ARCHIVE_INDEX_NAME: "antares_production_loci"

    ## Specify the chunk size for batch indexing into Elasticsearch.
    ##
    ARCHIVE_INDEX_MAX_CHUNK_SIZE: 1000

    ## Archive kafka consumer settings. The server/topic here should correspond to the
    ## server specified in pipeline.config.OUTPUT_KAFKA_SERVERS and
    ## pipeline.config.ARCHIVEINDEX_KAFKA_TOPIC_NAME.
    ##
    ARCHIVE_INDEX_KAFKA_TOPIC_NAME: ""
    ARCHIVE_INDEX_KAFKA_POLL_TIMEOUT: 1.0
    ARCHIVE_INDEX_KAFKA_GROUP: ""
    ARCHIVE_INDEX_KAFKA_SERVERS: ""
    ARCHIVE_INDEX_KAFKA_SSL_CA_LOCATION: ""
    ARCHIVE_INDEX_KAFKA_SASL_USER: ""
    ARCHIVE_INDEX_KAFKA_SASL_PASS: ""


## Configure the ANTARES pipeline.
##
pipeline:
  enabled: true
  name: "pipeline"
  image:
    repository: registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend
    tag: v1.10.0
    pullPolicy: IfNotPresent

  ## Pass extra environment variables to the containers of this application.
  ##
  extraEnv: []
  ## For example:
  #   - name: DATADOG_STATSD_HOST
  #     valueFrom:
  #       fieldRef:
  #         fieldPath: status.hostIP

  ## Configuration settings for the ANTARES Pipeline. These are the minimum set of config
  ## settings you'll need to pass in order to get up and running.
  ##
  config:
    DEBUG: false
    LOG_LEVEL: "INFO"

    ## Enter a full MySQL connection string, e.g. "mysql://user:pass@host:port/database"
    ##
    MYSQL_DB_URL: ""

    ## Use a redis-based locking mechanism to only process a single alert within a given
    ## region in the sky at a time.
    ##
    ENABLE_REGION_LOCK: true

    ## Disable any filters that crash. This sets their "enabled" column in the SFilter
    ## MySQL table to false and they must be re-enabled manually.
    ##
    PIPELINE_DISABLE_CRASHED_STAGES: true

    ## Raise errors if a pipeline crashes. Defaults to false to handle the failure and
    ## continue processing.
    ##
    PIPELINE_RAISE_STAGE_ERRORS: false

    ## Set pipeline outputs
    OUTPUTS: []
    ## For example, to output to a Kafka stream:
    #   - name: Nuclear Transients
    #     type: kafka
    #     criteria:
    #       has_tag: nuclear_transient
    #     topic: nuclear_transient

    ## Specify the domain that JWT authentication tokens are able to authenticate against.
    ## For example we set this value to ".antares.noirlab.edu" so that our API at
    ## "api.antares.noirlab.edu" can send a cross-domain cookie to users accessing the
    ## website at "antares.noirlab.edu".
    ##
    API_JWT_COOKIE_DOMAIN: ""

    ## Specify the base URL of the API. This is used to build relationship links in
    ## response payloads. It should be something like "https://api.antares.noirlab.edu/v1".
    ## You probably need the "/v1" suffix unless you know you don't.
    ##
    API_BASE_URL: ""

    ## Similar to the API_BASE_URL config option, this is used for building relationship
    ## links to views on the website. E.g. we set it to "https://antares.noirlab.edu".
    ##
    FRONTEND_BASE_URL: ""

    ## Cassandra configuration options.
    ##
    CASSANDRA_HOSTS: []
    CASSANDRA_USER: ""
    CASSANDRA_PASS: ""
    ## Set max number of concurrent C* queries.
    ##
    CASSANDRA_CONCURRENCY: 100
    CASSANDRA_ALERT_KEYSPACE: ""
    CASSANDRA_CATALOG_KEYSPACE: ""

    ## Configure the input Kafka broker, that is, the one that we pull alerts from.
    ##
    INPUT_KAFKA_SERVERS: ""
    INPUT_KAFKA_GROUP: ""
    INPUT_KAFKA_SASL_USER: ""
    INPUT_KAFKA_SASL_PASS: ""
    INPUT_KAFKA_SSL_CA_LOCATION: ""
    ## Specify the topics to consume from. This can either be a list of topics or the
    ## empty string. If the latter, ANTARES will autoconfigure the current evening's
    ## ZTF topic.
    INPUT_KAFKA_TOPICS: ""

    # Output Kafka
    OUTPUT_KAFKA_SERVERS: ""
    OUTPUT_KAFKA_SASL_USER: ""
    OUTPUT_KAFKA_SASL_PASS: ""
    OUTPUT_KAFKA_SSL_CA_LOCATION: ""
    OUTPUT_KAFKA_NEW_TOPIC_PARAMS:
      num_partitions: 1
      replication_factor: 1

    ## You must also specify the name of the index to be used in Elasticsearch for building
    ## the archive search database.
    ARCHIVE_ENABLE: true
    ARCHIVE_INDEX_KAFKA_TOPIC_NAME: ""


## DEPRECATED.
##
## The ANTARES rq-scheduler is used in conjunction with the rq-worker as a redis queue
## for running scheduled jobs. We leave it disabled by default here and, while we use it
## for internal jobs, its usage is not recommended nor can we support issues you may have
## using it.
##
rqScheduler:
  enabled: false
  name: "rq-scheduler"
  image:
    repository: registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend
    tag: v1.10.0
  extraEnv: []
  config: {}


## DEPRECATED.
##
## The ANTARES rq-worker is used in conjunction with the rq-scheduler as a redis queue
## for running scheduled jobs. We leave it disabled by default here and, while we use it
## for internal jobs, its usage is not recommended nor can we support issues you may have
## using it.
##
rqWorker:
  enabled: false
  name: "rq-worker"
  image:
    repository: registry.gitlab.com/nsf-noirlab/csdc/antares/antares/backend
    tag: v1.10.0
  extraEnv: []
  config: {}

